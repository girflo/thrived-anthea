var cropArea = $("#crop-area")
cropArea.hide();
var canvas = $("#canvas")
var success = $("#success")
var context = canvas.get(0).getContext("2d")
var fileSelector = $("#file-selector")
var preselectedImages = document.getElementsByClassName("preselected-image");
var audio = new Audio('/static/media/thrived-ambiant.mp3');

for (var i = 0; i < preselectedImages.length; i++) {
  preselectedImages[i].addEventListener('click', function(e) { loadPreselectedImage([e.currentTarget.src]); });
}

function loadPreselectedImage(src) {
  var filename = src.toString().split('/').pop()
  fetch(src).then(res => res.blob())
            .then(blob => {
              blob.name = filename
              loadFileInCropper(blob)
            })
}

function goToStep1() {
  var step0 = $("#step-0")
  var step1 = $("#step-1")
  step0.hide();
  step1.show();
  audio.play();
}

function loadFileInCropper(image) {
  var imageName = image.name;
  var reader = new FileReader();
  reader.onload = function(evt) {
    var img = new Image();
    img.onload = function() {
      context.canvas.height = img.height;
      context.canvas.width  = img.width;
      context.drawImage(img, 0, 0);
      canvas.cropper({
        aspectRatio: 1 / 1,
        cropBoxResizable: false,
        width: 500,
        height: 500
      });
      cropArea.show();
      fileSelector.hide();
      $('#button-crop').click(function() {
        cropArea.hide();
        var croppedCanvas = canvas.cropper('getCroppedCanvas', {width: 500, height: 500});
        croppedCanvas.toBlob(function (blob) {
          var formData = new FormData();
          formData.append('file', blob, imageName);
          $.ajax('/', {
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
              var xhr = new XMLHttpRequest();
              return xhr;
            },
            complete: function () {
              success.show()
            },
          });
        });
      });
    };
    img.src = evt.target.result;
  };
  reader.readAsDataURL(image);
}
