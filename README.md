# Thrived - Anthea 
## About
This app is a small upload server created with Flask in order to allow visitors to upload pictures of flowers. Those pictures will then be used to train a AI that will generate the album cover for the first record of Club Qu.

## Development
### Prerequisites
- [Install Flask](https://flask.palletsprojects.com/en/1.1.x/installation/)
- Create venv : `python3 -m venv venv` inside root folder
- Add Flask dependency `. venv/bin/activate` then `pip install Flask`

### Start dev server
``` shell
. venv/bin/activate
export FLASK_APP=thrived.py
flask run
```

## Production (Ubuntu)
### Prerequisites
- Nginx

### Install prod server

``` shell
sudo apt update
sudo apt install python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools
sudo apt install python3-venv
cd ~/thrived
virtualenv venv
source venv/bin/activate
pip install wheel
pip install gunicorn flask
gunicorn --bind 0.0.0.0:5000 wsgi:app
deactivate
mkdir upload
```

Create `/etc/systemd/system/thrived.service` with the following content :
``` shell
[Unit]
Description=Gunicorn instance to serve thrived
After=network.target

[Service]
User=forge
Group=www-data
WorkingDirectory=/home/forge/thrived.clubqu.com
Environment="PATH=/home/forge/thrived.clubqu.com/venv/bin"
ExecStart=/home/forge/thrived.clubqu.com/venv/bin/gunicorn --workers 3 --bind unix:thrived.sock -m 007 wsgi:app

[Install]
WantedBy=multi-user.target
```

Add the following section to your Nginx config : 
``` shell
    location / {
        include proxy_params;
        proxy_pass http://unix:/home/forge/thrived.clubqu.com/thrived.sock;
    }
``` 

### Start prod server 

``` shell
sudo systemctl start thrived
```

## Screenshot

### Step 0
![Step 0](./doc/screenshots/step_0.png)

### Step 1
![Step 1](./doc/screenshots/step_1.png)

### Step 2
![Step 2](./doc/screenshots/step_2.png)

### Step 3
![Step 3](./doc/screenshots/step_3.png)
